import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = { ProductsList:[] }

  componentDidMount(){
  const getList = async()=>{
    const response = await fetch('//localhost:8080/products/list')
    const ProductsList = await response.json()
    console.log("Our products are:",ProductsList)
    this.setState({ProductsList})
    }
  getList();
  }

  render() {
    const { ProductsList } = this.state
    return (
      
       
        ProductsList.map( product => 
          <div key={product.id}> 
              <p> {product.id} - {product.product_name} - {product.company} - {product.natural} - {product.contain_paracetamol} - {product.price}  </p>
          </div>
        )
    )
  }
}

export default App;
