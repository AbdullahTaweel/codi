import sqlite from 'sqlite'
import SQL from 'sql-template-strings'

const initializeDatabase = async () =>
{   const db = await sqlite.open('./db.sqlite');

    // const getProductsList = async () =>
    // {
    //     let returnString =""
    //     const rows = await db.all("SELECT product_id AS id, product_name, company, natural, contain_paracetamol, price FROM face_wash_coll ")
    //     rows.forEach (( { id, product_name, company, natural, contain_paracetamol, price}) => returnString+= `${id} - ${product_name} - ${company} - ${natural} - ${contain_paracetamol}- ${price}$`)
    //     return returnString
    // }
    const getProductsList = async () => {
        const rows = await db.all("SELECT product_id As id,product_name, company, natural, contain_paracetamol, price FROM face_wash_coll ")
        return rows
    }
        const controller = {
                            getProductsList
                           }
        
                        return controller
    }

    

export default initializeDatabase
// const test = async () =>{
//     const db = await sqlite.open('./db.sqlite');
//     await db.run(`CREATE TABLE face_wash_coll (product_id INTEGER PRIMARY KEY AUTOINCREMENT, product_name TEXT NOT NULL, company TEXT NOT NULL, natural BOOLEAN, contain_paracetamol BOOLEAN, price INTEGER)`)
 
// const stmt = await db.prepare(SQL `INSERT INTO face_wash_coll (product_name, company, natural, contain_paracetamol, price) VALUES (?, ?, ?, ?, ?)`)
//              await stmt.run (`Loreal`, `bla`, `true`, `false`, `10`);
//              await stmt.run (`Ursamajor`, `bla`, `true`, `false`, `8`);
//              await stmt.run(`Nivea`, `bla`, `true`, `false`, `4`);

//              await stmt.finalize();

//              const rows = await db.all("SELECT product_id AS id, product_name, company, natural, contain_paracetamol, price FROM face_wash_coll")
//              rows.forEach( ({ id, product_name, company, natural, contain_paracetamol, price }) => console.log(`[id:${id}] - ${product_name} - ${company} - ${ natural } - ${contain_paracetamol} - ${ price }`) )
//            }
           
//            export default { test }
           
        