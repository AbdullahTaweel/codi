// import { createServer } from 'http'
import app from './app'
//import db from './db'
import initializeDatabase from './db'


const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("Great"));

  app.get('/products/list', async (req, res) => {
  const ProductsList = await controller.getProductsList()
  console.log("Our products are:",ProductsList)
  res.json(ProductsList)
})
  
  app.listen(8080, () => console.log('your server is listening on 8080: Your Wishes are My Commands'))

// app.get('/', (req, res) => res.send("ok") );
// app.listen( 8080, () => console.log('server is listening on port 8080') )

}

start();